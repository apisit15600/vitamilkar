﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour
{
    public Transform target;
    public float speed =5f;
    // Start is called before the first frame update

    // Update is called once per frame
    private void Update()
    {
        Vector3 direction = target.position - transform.position;
        float angle=Mathf.Atan2(direction.x,direction.z)*Mathf.Rad2Deg;
       // Quaternion rotations =Quaternion.LookRotation(direction);
        //Quaternion rotation =Quaternion.AngleAxis(angle,Vector3.forward);
        transform.rotation=Quaternion.Euler(0,angle,0);
       // Debug.Log(angle);
    }
}
